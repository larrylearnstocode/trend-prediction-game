const labels = [];

const data = {
    labels: labels,
    datasets: [{
        label: '',
        backgroundColor: '#FAF4B7',
        fontColor: "#DAEAF1",
        pointRadius: 6,
        borderColor: '#F9F9F9',
        data: [],
    }]
};

const config = {
    type: 'line',
    data: data,
    options: {
        plugins: {
            legend: {
                display: false
            }
        },
        scales: {
            x: {
                ticks: {
                    color: '#DAEAF1'
                }
            },
            y: {
                ticks: {
                    color: '#DAEAF1'
                }
            }
        }
    }
};

const myChart = new Chart(document.getElementById('myChart'), config);

let bigD = myChart.config.data.datasets[0].data;

let goingUp = document.getElementById("up");
let goingDown = document.getElementById("down");
let message = document.getElementById("message");

let bettingUp = true;
let matchPoint = 0;
let betIsAlmostConfirmed = false;
let betIsConfirmed = false;
let moneyInThePool = 0;


function startEverything() {

    function generateRandomNumber() {

        let d = Math.floor((Math.random() * 100) + 1);
        // console.log("this is the new random numner" + d);
        // theLatestNum = d;
        addDataValue(d);

    }

    function addDataValue(num) {
        // let bigD = myChart.config.data.datasets[0].data;
        bigD.push(num);

        // console.log(bigD);
        if (bigD.length > 15) {
            bigD.shift();
        }
    }

    generateRandomNumber()

    let time = []

    function generateTimePoint() {
        let t = new Date()
        addTimeValue(t.toLocaleTimeString());
    }

    generateTimePoint()

    function addTimeValue(timeValue) {
        let bigT = myChart.config.data.labels;
        bigT.push(timeValue);

        // console.log(bigT);
        if (bigT.length > 15) {
            bigT.shift();
        }
    }

    if (bigD.length > 1) {
        matchPoint = bigD[bigD.length - 1] - bigD[bigD.length - 2]
        console.log(`the second last number: ${bigD[bigD.length - 2]}`);
        console.log(`the last number: ${bigD[bigD.length - 1]}`);
    } else {
        console.log("wait a second");
    }

    myChart.update();

}

setInterval(function () {
    startEverything();
    settlement();
}, 5000);




let balance = 0;
let funding = document.getElementById("inputBoxWager");
let wager = document.getElementById("fund")
let readyToWin = document.getElementById("readyToWin");


let wagerIsPlaced = false;
let displayWager = document.getElementById("displayWager");
let wagerNumber = 0;

function keydownFunction(e) {
    if (e.key === "Enter") {
        console.log("yess");

        console.log(funding.value);


        balance += parseInt(funding.value);
        wager.textContent = balance;
        funding.value = "";
        // document.getElementById("inputBoxWager").value = money.value;

    }
}

function bet(btnId, betAmount) {
    
    if (balance >= betAmount) {

        let bettingBox = document.getElementById(btnId);
        bettingBox.classList.add("glowing");

        balance = balance - betAmount;
        wager.textContent = balance;
        console.log(wager.textContent);
        wagerIsPlaced = true;

        // displayWager.textContent = 0;
        wagerNumber += betAmount;

        displayWager.textContent = ` ${wagerNumber}`;
    } else {
        alert("please add fund 😭😭");
    }
}


function betFive() {

    bet("five",5);

}

function betTen() {

    bet("ten",10);

}

function betHundred() {

    bet("hundred",100);

}

function betThousand() {

    bet("thousand",1000);

}


function choosingSide(upOrDown){

    if(upOrDown === "up" && wagerIsPlaced){
        goingUp.classList.toggle("choseASide");
        goingDown.classList.remove("choseASide");
        bettingUp = true;
        betIsAlmostConfirmed = true;
        console.log(`betisAlmostConfirmed is: ${betIsAlmostConfirmed}`)
    }else if(upOrDown === "down" && wagerIsPlaced){
        goingDown.classList.toggle("choseASide");
        goingUp.classList.remove("choseASide");
        bettingUp = false;
        betIsAlmostConfirmed = true;

        console.log(`betisAlmostConfirmed is: ${betIsAlmostConfirmed}`)

    }else {
        alert("please put your wager first")
    }
}

function buyingUp() {
    choosingSide("up");
}

function buyingDowm() {
    choosingSide("down");
}



function submitBet() {
    if (betIsAlmostConfirmed) {

        readyToWin.classList.add("confirmedBet");
        betIsConfirmed = true;
        console.log(betIsConfirmed);
        message.classList.add("message");
        message.classList.add("lightUp");
        if (bettingUp) {
            message.textContent = "your decision: UP 📈"
        } else {
            message.textContent = "your decision: DOWN 📉"
        }

    } else {
        alert("add fund and place your bet first");
    }

}

function settlement() {

    if (betIsConfirmed) {

        let betting5Box = document.getElementById("five");
        betting5Box.classList.remove("placingBet");
        betting5Box.classList.remove("glowing");

        let betting10Box = document.getElementById("ten");
        betting10Box.classList.remove("placingBet");
        betting10Box.classList.remove("glowing");

        let betting100Box = document.getElementById("hundred");
        betting100Box.classList.remove("placingBet");
        betting100Box.classList.remove("glowing");

        let betting1000Box = document.getElementById("thousand");
        betting1000Box.classList.remove("placingBet");
        betting1000Box.classList.remove("glowing");

        goingUp.classList.remove("choseASide");
        goingDown.classList.remove("choseASide");

        readyToWin.classList.remove("confirmedBet");
        message.classList.remove("lightUp");
        

        // message.classList.add("message");


        if (betIsConfirmed === true) {
            console.log(displayWager.textContent);
            if (matchPoint > 0 && bettingUp === true) {
                
                wager.textContent = parseInt(wager.textContent) + parseInt(displayWager.textContent) * 2;

                message.textContent = `you have won $ ${displayWager.textContent * 2}`
            
            } else if (matchPoint > 0 && bettingUp === false) {

                message.textContent = "you lost "

            } else if (matchPoint < 0 && bettingUp === true) {
 
                message.textContent = "you lost "


            } else if (matchPoint < 0 && bettingUp === false) {

                wager.textContent = parseInt(wager.textContent) + parseInt(displayWager.textContent) * 2;

                message.textContent = `you have won $ ${displayWager.textContent * 2}`
            }

            betIsConfirmed = false;
        } else {
            console.log("no bet yet");
        }
        wagerNumber = 0;
        displayWager.textContent = "";
        wagerIsPlaced = false;
        betIsAlmostConfirmed= false;

        // message.classList.remove("message");
    }

}

function setBalance(value){
    // update variable
    //update UI
}


function resetEverything() {
    console.log("i got it")
    while (myChart.config.data.labels.length > 0 || myChart.config.data.datasets[0].data.length > 0) {
        myChart.config.data.labels.pop();
        myChart.config.data.datasets[0].data.pop();
    }
    wager.textContent = 0;
    balance = 0;
    displayWager.textContent = 0;
    wagerNumber = 0;
    matchPoint = 0;
    let betting5Box = document.getElementById("five");
    betting5Box.classList.remove("glowing");
    let betting10Box = document.getElementById("ten");
    betting10Box.classList.remove("glowing");
    let betting100Box = document.getElementById("hundred");
    betting100Box.classList.remove("glowing");
    let betting1000Box = document.getElementById("thousand");
    betting1000Box.classList.remove("glowing");
    goingUp.classList.remove("choseASide");
    goingDown.classList.remove("choseASide");
    readyToWin.classList.remove("confirmedBet");
    message.classList.remove("message");
    message.textContent = "";

    startEverything();
}
